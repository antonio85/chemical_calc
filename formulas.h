#ifndef CLASSES_HPP_
#define CLASSES_HPP_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>
#include "elements.h"
#include "table.h"

using namespace std;

class Formulas
{
private:
        Table tableOfElements;
        Elements *periodic;

public:
        Formulas();
        void tokenize(string const &, const char , vector<string> &);
        bool checkNumber(string);
        string lookUpName(int);
        vector<string> getFormula(int);
        double molarMassCalc(const vector<string>);// when H 22 22 22 error
        int elementSearch(string);
        vector <string> sortElements(int, vector<double>&);
        double gramsToMoles(double, const vector<string>);
        double molesToGram(double, const vector<string>);
        double molaritySolution(double, double, const vector<string>);
        double howManyGramsMolarity(double , double , const vector<string>);
        void massPercentage(double, const vector<string>, vector<string> &, vector<double> &,vector<double> &);
};
#endif /* CLASSES_HPP_ */
