
#ifndef METHODS_H_
#define METHODS_H_

#ifdef _WIN32
#define sistema "windows"
#endif

#ifdef __APPLE__
#define sistema "linux"
#endif

#ifdef __unix
#define sistema "linux"
#endif
//preprocesors definitions to set get the OS where the program is being executed.
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>
#include "formulas.h"

using namespace std;

void menu(int);
void clearScreen();
string sortingResultToString(vector<string>,vector<double>);// gets a vector and transforms its contain into a string.
void printRecord(vector<string>);//print save records.
string formulaNameFromVector(vector<string>);
bool optionSave(vector<string> &a, string);// options to save results
template <class Number>//convert a number into
Number toString(Number);
bool againAndSave(vector<string> &a,string);// ask if we should repeat and save.
double checkDouble();
void saveToTextFile(vector<string> storage);

#endif /* METHODS_H_ */
