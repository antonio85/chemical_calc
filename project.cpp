#ifdef _WIN32
#define sistema "windows"
#endif

#ifdef __APPLE__
#define sistema "linux"
#endif

#ifdef __unix
#define sistema "linux"
#endif
//preprocesors definitions to set get the OS where the program is being executed.
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>
#include "formulas.h"
#include "methods.h"

using namespace std;

int main()
{
    Formulas operation;//creates the object operation from the class Formulas.
    bool again = true;
    char option;
    ostringstream os;//to write the results
    vector<string> storage;

    while (again)
    {

    	clearScreen();
    	menu(1);
    	cout << "Option #:";
    	cin >> option;// gets the option for the operation to perform
        cin.ignore(10000, '\n');
    	if (option == '1')// molar mass.
    	{
          string saveMolar;
      		bool againMolar = true;
      		while (againMolar)
      		{
    		      saveMolar.clear();
    		      vector <string> chemFormula;
        			clearScreen();
        			cout << "Introduce the chemical formula which Molar Mass you want to be calculated\n(For example, NaCl would need to be input as 'Na Cl', with a space between)." << endl;
        			chemFormula = operation.getFormula(2);//get formula
              string nameFormula = formulaNameFromVector(chemFormula);// formula to string
             	ostringstream os;//to write the results
        			double weight = operation.molarMassCalc(chemFormula);// get weight
        			os << "The molar mass of " << nameFormula << " is: " << weight <<"g/mol";// creates line
        			saveMolar =  os.str();// save it as string
        			againMolar = againAndSave(storage, saveMolar); // to save the results.
        		}
    	}
    	else if (option == '2')
    	{
        vector<double> formulaValues;
       	vector<string> sortedArray;
    		clearScreen();
    		bool againSort = true;// start true to enter to the loop, if event happens, change to false and stop loop
    		while(againSort)
    		{
    			bool entra = true;
    			while(entra)
    			{
    				clearScreen();
    				bool error = false;
    				if (!error)
    				{
    					cout << "Wrong Input. Try again.\n";
              error = true;
    				}
    				menu(2);
    				char sorting;
    				cin >> sorting;
    				cin.ignore(10000, '\n');
    				if(sorting=='1')
    				{
      					clearScreen();
      					cout <<"You have selected to sort by atomic number. Please, introduce the symbols of the elements you want to sort. "<< endl;
      					sortedArray = operation.sortElements(1, formulaValues);
      					string formulaName = sortingResultToString(sortedArray,formulaValues);
      					cout << endl;
               	ostringstream os;//to write the results
      					os << "The ascending order by atomic number is: " << formulaName<<" (atomic number or number of protons in the nucleus)."<<endl;
  		    			string saveMolar =  os.str();
  		    			cout << saveMolar << endl;
  		    			againSort = optionSave(storage, saveMolar); // to save need fix
    				}
    				else if(sorting == '2')
    				{
    				    clearScreen();
	              cout <<"You have selected to sort by molar mass. Please, introduce the symbols of the elements you want to sort. "<<endl;
	              sortedArray = operation.sortElements(2, formulaValues);
	              string formulaName = sortingResultToString(sortedArray,formulaValues);
	              cout << endl;
	              ostringstream os;//to write the results
	              os << "The ascending order by molar mass is: " << formulaName<<" (gr/mol)."<<endl;
	              string saveMolar =  os.str();
	              cout << saveMolar << endl;
	              againSort = optionSave(storage, saveMolar); // to save the results.

		    	  }
            else if (sorting == '3')
            {
                clearScreen();
                cout <<"You have selected to sort by electroNegativity. Please, introduce the symbols of the elements you want to sort. "<<endl;
                sortedArray = operation.sortElements(3, formulaValues);
                string formulaName = sortingResultToString(sortedArray,formulaValues);
                cout << endl;
                ostringstream os;//to write the results
                os << "The ascending order by electroNegativity is: " << formulaName<<" (electronegativity value)."<<endl;
                string saveMolar =  os.str();
                cout << saveMolar << endl;
                againSort = optionSave(storage, saveMolar); // to save the results.
            }
            else if (sorting == 'r' || sorting == 'R')
            {
           		againSort = false;
           		entra = false;
            }
            else
            {
            	error = true;
            }
    			}
    		}

    	}
	    else if (option == '3')
	    {
	          vector<string>formula;
	          double massGrams;// mass of grams of compouns
	          double numberMoles;// number of moles.
	          bool againMoles = true;
	          while (againMoles)
	          {
              bool entra = true;
              while(entra)
              {
    	            clearScreen();
    	            menu(4);
    	            char gramsOrMoles;
    	            cin >> gramsOrMoles;
    	            cin.ignore(10000, '\n');
                  againMoles = false;

    	            if (gramsOrMoles == 'G' || gramsOrMoles == 'g')
    	            {
    	              clearScreen();
    	              cout << "Introduce the mass of the compound (in grams) and the formula of the compound. \n";
    	              cout << "Mass in grams: ";
    	              bool correct;
                    massGrams = checkDouble();
                  	cout << "Now, please provide the chemical formula of the compound: \n";
                    formula = operation.getFormula(2);
                    double numberOfMoles = operation.gramsToMoles(massGrams, formula);
                    string formulaName = formulaNameFromVector(formula);
                    ostringstream os;//to write the results

                    os << "The number of moles in " << massGrams << "g of " <<  formulaName << " is " << numberOfMoles<<" moles.";
                    string saveMolar =  os.str();//convert to string
                    cout << endl;
                    againMoles = againAndSave(storage, saveMolar);
    	            }
    	            else if (gramsOrMoles == 'M' || gramsOrMoles == 'm')
    	            {
    	              clearScreen();
    	              cout << "Introduce the number of Moles and the formula of the compound. \n";
    	              cout << "# Moles: ";
    	              bool correct;
                    numberMoles = checkDouble();
    	              cout << "Now, please provide the chemical formula of the compound: \n";
    	              formula = operation.getFormula(2);
    	              double gramsFromMoles = operation.molesToGram(numberMoles, formula);
    	              string formulaName = formulaNameFromVector(formula);
    	              ostringstream os;//to write the results
    	              os << "The number of grams in " << numberMoles << " moles of " <<  formulaName << " is " << gramsFromMoles << "g.";
    	              string saveMolar =  os.str();
                    cout << endl;
    	              againMoles = againAndSave(storage, saveMolar);
    	            }
                  else if (gramsOrMoles == 'R' || gramsOrMoles == 'r')
                  {
                    againMoles = false;
                    entra = false;
                  }

                  else
    	            {
    	              againMoles = true;
    	            }
              }
	          }
	      }

      else if(option == '4')
      {
          bool againComposition = true;
          while(againComposition == true)
          {
              clearScreen();
              vector<string> formulaPercentage;
              cout << "Compound percentage composition.\nFirst, Enter the mass of the compound (in grams) to analize its percentage of elements: ";
              double massComposition = checkDouble();
              cout << "Now, provide the formula of the compound: ";
              formulaPercentage = operation.getFormula(2);
              vector<string> elementList;
              vector<double> massElementPresent;
              vector<double> percentageList;
              operation.massPercentage(massComposition, formulaPercentage,elementList, massElementPresent,percentageList);// it takes a vector contained a formated formula and the mass, it returns three pointers to vectors with data.
              string formulaName = formulaNameFromVector(formulaPercentage);
              string text = "";
              int i = 0;
              ostringstream os;//to write the results
              while(i<elementList.size())
              {
                  os << setprecision (5) << percentageList[i] << "% ("  << massElementPresent[i]  << "g) of " << operation.lookUpName(operation.elementSearch(elementList[i]));

                  if(i == elementList.size()-2)
                  {
                    os << " and ";
                  }
                  else if(i == elementList.size()-1)
                  {
                    os << ".";
                  }
                  else
                  {
                    os << ", ";
                  }
                  i++;
              }
              string unionText =  os.str();
              //cout << unionText;
              //clearScreen();
              ostringstream result;
              result << "The corresponding composition of " << massComposition <<"g of "<< formulaName << " is " << unionText;
              string respuesta =  result.str();
              againComposition = againAndSave(storage, respuesta);

            }
      }

      else if(option == '5')
      {
        vector <string> formulaMolarity;
        bool againMolarityP = true;
        while(againMolarityP)
        {
          bool correct = true;
          while (correct)
          {
            clearScreen();
            menu(5);
            char optionMolarity;
            cin >> optionMolarity;
            cin.ignore(10000, '\n');
            if(optionMolarity == '1')
            {
              clearScreen();
              formulaMolarity.clear();
              cout << "Find the molarity. \n";
              cout << "Please, indicate the volumen of the solution(in Liters): ";
              double volumen = checkDouble();
              cout << "Now, indicate mass of the solute(in Grams): ";
              double massSolute = checkDouble();
              cout << "Finaly, indicate the chemical Formula:\n";
              formulaMolarity = operation.getFormula(2);// get the formula fortamted to be processed
              string formula = formulaNameFromVector(formulaMolarity);
              double molarityTotal = operation.molaritySolution(massSolute, volumen, formulaMolarity);
              correct = false;
              ostringstream resultMolarity;
              resultMolarity << "The molarity of the " << formula << " solution is "<<molarityTotal<<"M.";
              string result =  resultMolarity.str();
              againMolarityP = againAndSave(storage, result);
            }
            else if(optionMolarity == '2')
            {
              clearScreen();
              formulaMolarity.clear();
              cout << "Find the mass of solute. \n";
              cout << "Please, indicate the volumen of the solution(in Liters): ";
              double volumen = checkDouble();
              cout << "Now, indicate molarity(M) of the solution: ";
              double molaritySolution = checkDouble();
              cout << "Finaly, indicate the chemical Formula:\n";
              formulaMolarity = operation.getFormula(2);// get the formula fortamted to be processed
              string formula = formulaNameFromVector(formulaMolarity);
              double molarityTotal = operation.howManyGramsMolarity(molaritySolution, volumen, formulaMolarity);
              cout << "Find the mass of solute present in the solution: \n";
              ostringstream resultWeight;
              resultWeight << "The weight of solute in " << molaritySolution << "M of a solution of " << formula << " is " << molarityTotal<<"M.";
              string result =  resultWeight.str();
              againMolarityP = againAndSave(storage, result);
            }

          }

        }
      }
      else if(option == '6')
      {
        printRecord(storage);
        cout << "Press 'P' to output the result in a text file or Any key go back to main menu. \n";
        char key;
        cin >> key;
        cin.ignore(10000, '\n');
        if (key == 'P' || key == 'p')
        {
          saveToTextFile(storage);
          cout << "Saved!\n";
          cout << "Save! It's located in the source directory\n";
        }
      }
      else if(option == 'q' || option == 'Q')
      {
        clearScreen();
        again = false;
      }
      else
      {
        again = true;
      }

    }

    return 0;
 }
