
#ifndef TABLE_HPP_
#define TABLE_HPP_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>
#include "elements.h"

using namespace std;

const int TOTAL_NUMBER_ELEMENTS = 118;

class Table
{
private:
        Elements periodicTable [TOTAL_NUMBER_ELEMENTS];
public:
        Table();
        void passInfo();
        Elements* getTable();
};
#endif /* TABLE_HPP_*/