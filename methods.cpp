#include "methods.h"

void menu(int menu)
{
    switch(menu){
        case 1:
        cout << " Chemical calculator. Please, select what operation you would like to perform:\n";
        cout << " * Press '1' for calculate the molar mass.\n";
        cout << " * Press '2' for sorting elements by molar mass, atomic number or electronegativit.\n";
        cout << " * Press '3' to transform from moles to grams or grams to moles of a compound.\n";
        cout << " * Press '4' to obtain the mass composition of a compound.\n";
        cout << " * Press '5' to Calculate the Molarity and how many grams of a compound contains. \n";
        cout << " * Press '6' to check all the calculations you have saved.\n";
        cout << " * 'Q' to quit\n";
        break;

        case 2:
        cout << "Please select the sorting method: \n";
        cout << " * Press '1' : by atomic number.\n";
        cout << " * Press '2' : for atomic mass.\n";
        cout << " * Press '3' : for electronegativity.\n";
        cout << " * Press 'R' to go back.\n";
        break;

        case 3:
        cout << " * Press 'Y' for new molar calculation.\n * Press 'S' to save the result and continue.\n * Press 'W' to save and return to main menu.\n * Press 'R' to go back without saving.\n";
        break;

        case 4:
        cout<< "Select:\n * Press 'G' for grams to moles. \n * Press 'M' for moles to grams.\n * Press 'R' to go back.\n";
        break;

        case 5:
        cout << "Please, choose between the following options.\n";
        cout << " * Press '1' if you want to know the molarity of a solution. \n";
        cout << " * Press '2' if you want to know how many grams of a compound is present in a solution.\n";
        cout << " * Press 'R' to go back.\n";
    }
}

void clearScreen()
{// checks what system is being used in order to choose the right screen cleaner command. script. I tried it in three different system. (Ubuntu 64 18.4, Windows 10 and Mac OS mojave.)
	string hola = sistema;
	if (hola == "linux")
	{
		system("clear");
	}
	else
	{
		system("CLS");
	}
}

string sortingResultToString(vector<string> symbols, vector<double> values)// gets a vector and transforms its contain into a string to be used in the sorting fuction. 2 for formula name
{
	string result;
  int len = symbols.size();
  ostringstream os;//to write the results
	for(int i = 0; i<len; i++ )
	{
		os<<setprecision(3)<<symbols[i]<<"("<<values[i]<<")";

    if(i<len-1)
    {
      os<<" -> ";
    }
	}
  result =  os.str();
	return result;
}

void printRecord(vector<string> storage)//prints the content of a vector
{
    cout << "Storage: "<<endl;
		for(auto i = storage.begin(); i != storage.end(); i++)
    {
      cout << *i;
      cout << endl << endl;
    }
}

string formulaNameFromVector(vector<string> record)//takes a vector and creates a string with a formula name.
{
    string formulaName;
		for(int i = 0; i < record.size(); i++)
    {
      if(record[i]!="1")
        formulaName.append(record[i]);
    }
    return formulaName;
}

bool optionSave(vector<string> &a,string save)// options to save results
{
	menu(3); //options for saving.
	bool againToDo = true;
  bool againOption;
	char again;
	while (againToDo)
	{
		cin >> again;
    cin.ignore(10000, '\n');
    if (again == 'Y' || again =='y')//continue without saving
    {
      againOption = true;
      againToDo = false;
    }
    else if (again == 'S' || again =='s')//save and continue
    {
      againOption = true;
      a.push_back(save);
      againToDo = false;
    }
    else if (again == 'W' || again == 'w')//save and exit
    {
      againOption = false;
      a.push_back(save);
      againToDo = false;
   	}

    else if (again == 'r' || again == 'R')//exit without saving
    {
       	againOption = false;
       	againToDo = false;
  	}
    else
    {
	      cout << "Wrong option. Try again." << endl;
        againToDo = true;
    }
  }


	return againOption;
}

bool againAndSave(vector<string> &a, string saveResult)// ask if we should repeat and save.
{
    cout << endl << saveResult << endl;
    bool again;
    again = optionSave(a,saveResult);
    return again;
}

double checkDouble()//checks if a number is a double or ask another one
{//if the value is wrong it will ask for a new one and reset the warning signal for the cinm.
  double input;
  cin >> input;
  while(!cin)
  {// catches bad inputs
      cout << "Bad option! Please, try again. ";
      cin.clear();
      cin.ignore(10000, '\n');
      cin >> input;
  }
  cin.ignore(10000, '\n');

  return input;
}


void saveToTextFile(vector<string> storage)
{

  ofstream myfile ("result.txt");
  if (myfile.is_open())
  {
    for(auto i = storage.begin(); i != storage.end(); i++)
    {
      myfile << *i << endl;
    }
    myfile.close();
  }
  else cout << "Unable to open file";


}
