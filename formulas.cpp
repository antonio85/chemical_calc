#include "formulas.h"

//const int TOTAL_NUMBER_ELEMENTS = 118;// there is 118 elements in the periodic table.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
//  Elements will be our data structure that will hold all the values that we will need for                 //
//  performing the calculations. I wanna create an static array of classes (Elements) that will represent   //
//  the elements in the periodic table. Another class (Operations) will initialize Elements with their      //
//  respective components and it will have the fuctions to manipulate the data inside the Elements          //
//  data structure.                                                                                         //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Formulas::Formulas()
    {
    periodic = tableOfElements.getTable();

    }

    void Formulas::tokenize(string const &str, const char delim, vector<string> &out)
    {
        size_t start;
        size_t end = 0;
        while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
        {
            end = str.find(delim, start);
            out.push_back(str.substr(start, end - start));
        }
    }

    bool Formulas::checkNumber(string number)//verify if the string value is a number
    {
        bool isNumber =true;
            for(int i =0; i<number.length(); ++i){

                if(isdigit(number[i])==false ){
                    isNumber = false;
                 }
            }
            return isNumber;
    }

    string Formulas::lookUpName(int index)//returns a string with the  name of a element.
    {
      return periodic[index].getName();
    }

    vector<string> Formulas::getFormula(int instrucciones)// logic the receive a chemical formula. It will check if the formula is syntatically correct.
    {
        char delimiter = ' ';
        string linea;
        vector<string> formu;
        vector<string> formuFinal;
        string tok;
        bool valido = false;
        int again = 0;
        bool salir = true;

        if (instrucciones == 1)
        {
            while (valido == false)
            {
                if (again == 1)
                {
                    formu.clear();
                    formuFinal.clear();
                    cout << "Bad imput. Please, try again. \n";
                    again = 0;
                }
                else if(again == 2)
                {
                    formu.clear();
                    formuFinal.clear();
                    cout << "Empty input is now allowed!\n";
                    again = 0;
                }

                salir  = true;
                getline(cin, linea);
                //cin.ignore(10000, '\n');
                tokenize(linea, delimiter, formu);
                int tamano = formu.size();
                valido = true;
                int i = 0;

                if (tamano == 0)
                {
                  valido = false;
                  again = 2;
                }

                while(i<tamano)
                {
                    if (elementSearch(formu[i]) != -1)
                    {
                        formuFinal.push_back(formu[i]);
                    }
                    else
                    {
                        valido = false;
                        again = 1;
                        break;
                    }
                    i++;
                }
            }
        }


        else if ( instrucciones == 2)
        {

            while (valido == false)
            {
                if (again == 1)
                {
                    formu.clear();
                    formuFinal.clear();
                    cout << "Bad imput. Please, try again1. \n";
                    again = 0;
                }
                else if(again == 2)
                {
                    formu.clear();
                    formuFinal.clear();
                    cout << "Empty input is now allowed!\n";
                    again = 0;
                }
                getline(cin, linea);
                //cin.ignore(10000, '\n');
                tokenize(linea, delimiter, formu);
                valido = true;
                int i = 0;
                int tamano = formu.size();

                if (tamano == 0)
                {
                  valido = false;
                  again = 2;
                }

                while(i<tamano)
                {
                    if(elementSearch(formu[0]) == -1)
                    {
                        valido = false;
                        again = 1;
                        break;
                    }
                    if (i+1<tamano)
                    {
                        if(elementSearch(formu[i]) != -1 && elementSearch(formu[i+1]) != -1){
                            formuFinal.push_back(formu[i]);
                            formuFinal.push_back("1");
                        }
                        else if (elementSearch(formu[i]) != -1)
                        {
                            formuFinal.push_back(formu[i]);

                        }
                        else if(checkNumber(formu[i]) == true)
                        {
                            formuFinal.push_back(formu[i]);
                        }
                    }
                    else{
                        if(elementSearch(formu[i]) != -1)
                        {
                            formuFinal.push_back(formu[i]);
                            formuFinal.push_back("1");
                        }
                        else if(checkNumber(formu[i]) == true)
                        {

                            formuFinal.push_back(formu[i]);

                        }
                        else{
                            valido = false;
                            again = 1;
                        }
                    }
                    i++;
                }
            }
        }
        return formuFinal;
    }

    double Formulas::molarMassCalc(const vector<string> formula)// calculate the weight of a chemical formula.
    {
        int coefficient = 0;//generally an integer
        double massElement = 0;//a double
        double total = 0;//hold result
        int length = formula.size();// length of the chemical formula
        int i = 0;
        while (i<length)// the vector has the formula some form that the program can read its contains and process it.
        {
            massElement = periodic[elementSearch(formula[i])].getAtomicMass();// first element is always an element. Gets the molar mass.
            coefficient = atoi(formula[i+1].c_str()); // following number is a coefficient.
            double temp = massElement * coefficient; // molar mass times coeficcient.
            total = total +  temp;
            i=i+2;
        }
        return total;
    }

    int Formulas::elementSearch(string symbol)//find the index where the element is located. If the string is not a chemical symbol, returns -1.
    {
        for(int i =0; i<118; i++)
        {
            if (periodic[i].getSymbol() == symbol)
            {
                return i;
            }
        }
        return -1;
    }

    vector <string> Formulas::sortElements(int field, vector<double> &formulaValues)//sort requested elements according to user's desire
    {
        vector <string> formula = getFormula(1);
        int pivotOne = 0;
        int length = formula.size();

        if (field == 1)// by atomic number
        {
            while ( pivotOne < length)
            {
                int pivotTwo = pivotOne + 1;

                while (pivotTwo < length)
                {
                    if(periodic[elementSearch(formula[pivotOne])].getAtomicNumber() > periodic[elementSearch(formula[pivotTwo])].getAtomicNumber())
                    {
                        string temp = periodic[elementSearch(formula[pivotOne])].getSymbol();
                        formula[pivotOne] =  formula[pivotTwo];
                        formula[pivotTwo] = temp;
                    }
                    pivotTwo++;
                }
                pivotOne++;
            }
            // will hold the values of the elements requested.
            for(int i = 0; i< formula.size(); i++)
            {
                formulaValues.push_back(periodic[elementSearch(formula[i])].getAtomicNumber());
            }
        }
        else if ( field == 2)//by atomic mass
        {
            while ( pivotOne < length)
            {
                int pivotTwo = pivotOne + 1;

                while (pivotTwo < length)
                {
                    if(periodic[elementSearch(formula[pivotOne])].getAtomicMass() > periodic[elementSearch(formula[pivotTwo])].getAtomicMass())
                    {
                        string temp = periodic[elementSearch(formula[pivotOne])].getSymbol();
                        formula[pivotOne] =  formula[pivotTwo];
                        formula[pivotTwo] = temp;
                    }
                    pivotTwo++;
                }
                pivotOne++;
            }

            for(int i = 0; i< formula.size(); i++)
            {
                formulaValues.push_back(periodic[elementSearch(formula[i])].getAtomicMass());
            }
        }
        else if ( field == 3)// by electronegativit.
        {
            /* code */
            while ( pivotOne < length)
            {
                int pivotTwo = pivotOne + 1;

                while (pivotTwo < length)
                {
                    if(periodic[elementSearch(formula[pivotOne])].getElectroNegativity() > periodic[elementSearch(formula[pivotTwo])].getElectroNegativity())
                    {
                        string temp = periodic[elementSearch(formula[pivotOne])].getSymbol();
                        formula[pivotOne] =  formula[pivotTwo];
                        formula[pivotTwo] = temp;
                    }
                    pivotTwo++;
                }
                pivotOne++;
            }

            for(int i = 0; i< formula.size(); i++)
            {
                formulaValues.push_back(periodic[elementSearch(formula[i])].getElectroNegativity());
            }
        }

        return formula;// returns symbols, and it returns also the values by reference.
    }

    double Formulas::gramsToMoles(double grams, const vector<string> formula)
    {
        return  double(grams) / molarMassCalc(formula);
    }

    double Formulas::molesToGram(double moles, const vector<string> formula)
    {
        return moles * molarMassCalc(formula);
    }

    double Formulas::molaritySolution(double grams, double liters, const vector<string> chemForm)// molarity in a solution
    {
       double numeroOfMoles = gramsToMoles(grams,chemForm);
       double molarityInSolution = numeroOfMoles / liters;
       return molarityInSolution;
    }

    double Formulas::howManyGramsMolarity(double molarity, double liters, const vector<string> chemForm)
    {
      double molesSolute = molarity / liters;
      double gramsWeight = molesSolute * molarMassCalc(chemForm);
      return gramsWeight;
    }


    void Formulas::massPercentage(double mass, const vector<string> chemForm, vector<string> &elementList, vector<double> &massElementPresent,vector<double> &percentageList)// it takes a vector contained a formated formula and the mass, it returns three pointers to vectors with data.
    {
      double molarMassTotalPercentage = molarMassCalc(chemForm);

      int i = 0;
      while(i<chemForm.size())
      {
        double temp = periodic[elementSearch(chemForm[i])].getAtomicMass();
        double coefficient = atoi(chemForm[i+1].c_str());
        temp = temp * coefficient;
        elementList.push_back(chemForm[i]);
        double value = temp/molarMassTotalPercentage;
        percentageList.push_back(value*100);
        massElementPresent.push_back(mass*value);
        i+=2;
      }
    }
