#ifndef ELEMENTS_HPP_
#define ELEMENTS_HPP_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>

#include "elements.h"

using namespace std;

//const int TOTAL_NUMBER_ELEMENTS = 118;// there is 118 elements in the periodic table.

class Elements// hold one element and its data. The idea is create an array with 118 objects of Elements to hold each element of the periodic table and its data.
{
//some members of the class, will add more later.
private:
    string name;// name of the elements
    string symbol;
    string group;
    int atomicNumber;//number of protons
    int numberOfNeutrons;// mass number - protons.
    double  atomicWeight;//the avera
    double electroNegativity;

public:
    //setters to add the info into the data structure. *add more later
    void setName(string);
    void setSymbol(string);
    void setGroup(string);
    void setAtomicNumber(int);// always an integer
    void setAtomicMass(double);
    void setElectroNegativity(double);
    //getters to access the private members of the class.
    string getName();
    string getSymbol();
    string getGroup();
    int getAtomicNumber(); // atomic number is always an integer
    double getAtomicMass();
    double getElectroNegativity();
    int getNumberOfNeutrons();
};

#endif /* ELEMENTS_HPP_ */
