
#include "table.h"

Table::Table()
{
    passInfo();//build the periodic table and enter some variables
}

void Table::passInfo()//method to obtain the data from the text file and add them to the data structure.
{
    ifstream infile("file.txt");// open  the file where the elements data is held.
    string arr[118][5];// array hold the values, as the data struct would do.
    string linea;

    if(infile.is_open())
    {

        int i = 0;
        while(getline(infile, linea))// extract the line and save it in the array
        {
            int j = 0;
            stringstream ssin(linea);
            while (ssin.good() && j < 5)
            {
                ssin >> arr[i][j];
                ++j;
            }
            ++i;
        }
        infile.close();// close the file
    }

    for(int i = 0; i<118; i++)
    {
        periodicTable[i].setName(arr[i][0]);
        periodicTable[i].setSymbol(arr[i][1]);
        periodicTable[i].setAtomicNumber(atoi(arr[i][2].c_str()));
        periodicTable[i].setAtomicMass(strtof((arr[i][3]).c_str(), 0));
        periodicTable[i].setElectroNegativity(strtof((arr[i][4]).c_str(), 0));
    }

    /*for(int i = 0;i<118; i++)
    {
    cout << "Probando: " << periodicTable[i].getName() << " " << periodicTable[i].getSymbol() <<
             " " << periodicTable[i].getAtomicNumber() << " " << periodicTable[i].getAtomicMass() << " " << periodicTable[i].getNumberOfNeutrons() << endl;
    }*/
}

Elements* Table::getTable()
{
    return periodicTable;
}
