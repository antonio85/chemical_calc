
#include "elements.h"


void Elements::setName(string name)
{
    this-> name = name;
}

void Elements::setSymbol(string symbol)
{
    this-> symbol = symbol;
}

void Elements::setGroup(string group)
{
    this-> group = group;
}

void Elements::setAtomicNumber(int atomicNumber)// always an integer
{
    this-> atomicNumber = atomicNumber;
}

void Elements::setAtomicMass(double atomicWeight)
{
    this-> atomicWeight = atomicWeight;
}

void Elements::setElectroNegativity(double electroNegativity)
{
    this->electroNegativity = electroNegativity;
}

//getters to access the private members of the class.
string Elements::getName()
{
    return name;
}

string Elements::getSymbol()
{
    return symbol;
}

string Elements::getGroup()
{
    return group;
}

int Elements::getAtomicNumber() // atomic number is always an integer
{
    return atomicNumber;
}

double Elements::getAtomicMass()
{
    return atomicWeight;
}

double Elements::getElectroNegativity()
{
    return electroNegativity;
}
int Elements::getNumberOfNeutrons()
{
    return atomicWeight-atomicNumber;
}
